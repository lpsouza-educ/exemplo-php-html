<!DOCTYPE html>
<?php
    $titulo = "Página PHP do sor Luiz";

    $nomes = array(
        "Luiz",
        "Ana",
        "Manoel",
        "Helena"
    );

    $nascimento = array(1986, 9, 10);

    $hoje = date_parse(date('Y-m-d'));

?>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $titulo; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h1><?php echo $titulo; ?></h1>
    <h2>Listagem de nomes:</h2>
    <ul>
        <?php for ($i=0; $i < count($nomes); $i++) { ?>
        <li><?php echo $nomes[$i]; ?></li>
        <?php } ?>
    </ul>
    <h2>Listagem de nomes (invertida):</h2>
    <ul>
        <?php for ($i=count($nomes)-1; $i >= 0 ; $i--) { ?>
        <li><?php echo $nomes[$i]; ?></li>
        <?php } ?>
    </ul>
    <h2>Validando se uma idade é maior de 18:</h2>
    <?php
        $idade = $hoje['year'] - $nascimento[0];
        if ($hoje['month'] < $nascimento[1]) {
            $idade--;
        } else if ($hoje['month'] == $nascimento[1] && $hoje['day'] < $nascimento[2]) {
            $idade--;
        }
    ?>
    <p><?php if ($idade < 18) {
        echo "Você é de menor!";
    }else {
        echo "Você é de maior!";
    } ?></p>
</body>
</html>
